package org.automationpractice.base;
import org.automationpractice.base.DriverClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.By;

public class Actions {
    public WebDriver driver;
    private WebDriverWait wait;
    private long GLOBAL_TIMEOUT = 60;

    public Actions(){
        DriverClass driverClass = DriverClass.getInstanceOfSingletonBrowserClass();
        this.driver = driverClass.getDriver();
        this.wait = new WebDriverWait(driver, GLOBAL_TIMEOUT);
    }
    public void clickElement(By element){
        this.wait.until(ExpectedConditions.elementToBeClickable(element));
        this.driver.findElement(element).click();
    }
    public void setText(By element, String text) {
        WebElement webElement = this.driver.findElement(element);
        this.wait.until(ExpectedConditions.visibilityOf(webElement));
        webElement.sendKeys(text);
    }
    public String getElementText(By element){
        WebElement webElement = this.driver.findElement(element);
        this.wait.until(ExpectedConditions.visibilityOf(webElement));
        return webElement.getText();
    }
    public Boolean isElementDisplayed(By element){
        WebElement webElement = this.driver.findElement(element);
        this.wait.until(ExpectedConditions.visibilityOf(webElement));
        return webElement.isDisplayed();
    }

}
