package org.automationpractice.base;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class DriverClass {

    // instance of singleton class
    private static DriverClass instanceOfSingletonBrowserClass=null;


    private WebDriver driver;

    // Constructor
    private DriverClass(){
        System.setProperty("webdriver.chrome.driver","/Users/danielmc/testautomation/chromedriver");
        driver= new ChromeDriver();
    }

    // TO create instance of class
    public static DriverClass getInstanceOfSingletonBrowserClass(){
        if(instanceOfSingletonBrowserClass==null){
            instanceOfSingletonBrowserClass = new DriverClass();
        }
        return instanceOfSingletonBrowserClass;
    }

    public void quitDriver()
    {
        driver.quit();
    }
    // To get driver
    public WebDriver getDriver()
    {
        return driver;
    }
}