package org.automationpractice.pages;
import org.automationpractice.base.Actions;
import org.openqa.selenium.By;


public class BreadCrumbs extends Actions{

    By spnSearchBreadCrumb = By.xpath("// span[ text() = \"Search\" and contains(@class, 'navigation_page')]");

    public BreadCrumbs(){

    }

    public Boolean isSearchBreadcrumbEnabled(){
        return super.isElementDisplayed(spnSearchBreadCrumb);
    }
}

