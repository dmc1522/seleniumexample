package org.automationpractice.pages;
import org.automationpractice.base.Actions;
import org.openqa.selenium.By;


public class HeaderPage extends Actions {
    By txtSearch = By.id("search_query_top");
    By btnSearch = By.className("button-search");

    public void searchItem(String itemName){
        super.setText(txtSearch, itemName);
        super.clickElement(btnSearch);
    }
}
