package org.automationpractice;
import org.automationpractice.base.DriverClass;
import org.automationpractice.pages.BreadCrumbs;
import org.automationpractice.pages.LoginPage;
import org.testng.annotations.*;
import org.automationpractice.pages.HeaderPage;

public class CreateUserTests {

    private HeaderPage headerPage;
    private BreadCrumbs breadCrumbs;
    private LoginPage loginPage;

    @BeforeClass
    public void setUp() {
        headerPage = new HeaderPage();
        breadCrumbs = new BreadCrumbs();
        loginPage = new LoginPage();
        loginPage.Login();
    }

    @Test
   public void searchItem(){
        try{
            String searchCriteria = "dress";
            headerPage.searchItem(searchCriteria);
            assert breadCrumbs.isSearchBreadcrumbEnabled();
            Thread.sleep(10 * 1000);
       }catch(Exception ex){
            System.out.println("Error while doing stuff "+ ex);
        }
   }

   @AfterClass
    public void tearDown(){
       DriverClass driver = DriverClass.getInstanceOfSingletonBrowserClass();
       driver.quitDriver();

    }
}
