package org.automationpractice;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.automationpractice.pages.BreadCrumbs;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import org.automationpractice.pages.HeaderPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class CreateUserTests {

    private HeaderPage headerPage;
    private WebDriver driver;
    private  BreadCrumbs breadCrumbs;

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver","/Users/danielmc/testautomation/chromedriver");
        //Create driver object for Chrome
        driver = new ChromeDriver();
        headerPage = new HeaderPage(driver);
        breadCrumbs = new BreadCrumbs(driver);
        driver.get("http://automationpractice.com/index.php");

    }

//    @After
//    public void tearDown(){
//        driver.quit();
//    }

    @Test
   public void searchItem(){
        try{
            String searchCriteria = "dress";
            headerPage.searchItem(searchCriteria);
            assert breadCrumbs.isSearchBreadcrumbEnabled();
            Thread.sleep(10000);
       }catch(Exception ex){
            System.out.println("Error while doing stuff "+ ex);
        }
   }
}
