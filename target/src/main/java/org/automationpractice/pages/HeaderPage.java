package org.automationpractice.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.By;


public class HeaderPage {
    private WebDriver driver;
    private WebDriverWait wait;

    By txtSearch = By.id("search_query_top");
    By btnSearch = By.className("button-search");
    // span[ text() = "Search" and contains(@class, 'navigation_page')]

    public HeaderPage(WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(driver,5);
    }

    public void searchItem(String itemName){
        driver.findElement(txtSearch).sendKeys(itemName);
        driver.findElement(btnSearch).click();
    }
}

