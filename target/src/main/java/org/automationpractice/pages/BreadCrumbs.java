package org.automationpractice.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.By;


public class BreadCrumbs {
    private WebDriver driver;
    private WebDriverWait wait;

    By spnSearchBreadCrumb = By.xpath("// span[ text() = \"Search\" and contains(@class, 'navigation_page')]");


    public BreadCrumbs(WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(driver,5);
    }

    public Boolean isSearchBreadcrumbEnabled(){

        //WebElement breadCrumb = wait.until(ExpectedConditions.presenceOfElementLocated(spnSearchBreadCrumb));
        boolean  isElementDisplayed = driver.findElement(spnSearchBreadCrumb).isDisplayed();
        return isElementDisplayed;

    }
}

